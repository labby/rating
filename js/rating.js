/**
 *  @module         rating
 *  @version        see info.php of this module
 *  @authors        erpe
 *  @copyright      2010-2018 erpe
 *  @license        MIT
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *
 */

jQuery(document).ready(function($) {
	$('.status').prepend("<div class='score_this'>(<a href='#'>Score this item ...</a>)</div>");
	$('.score_this').click(function(){
		$(this).slideUp();
		return false;
	});
	
	$('.score a').click(function() { 
		// alert ("call ...");
		$(this).parent().parent().parent().addClass('scored');
		$.get("rating/rating.php" + $(this).attr("href") +"&update=true", {}, function(data){
			$('.scored').fadeOut("normal",function() {
				$(this).html(data);
				$(this).fadeIn();
				$(this).removeClass('scored');
			});
		});
		return true; // drp: was 'false'!
	});
});