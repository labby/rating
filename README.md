### rating
============

Snippet to add rating to your pages usind a droplet.

#### Requirements

* [LEPTON CMS][1], Version >= 4.0


#### Installation

* download latest [.zip][2] installation archive
* in CMS backend select the file from "Add-ons" -> "Modules" -> "Install module"

#### Notice

After installing addon you are done. <br />
Please see documentation on [LEPAdoR][3]



[1]: https://lepton-cms.org "LEPTON CMS"
[2]: http://www.lepton-cms.com/lepador/snippets/rating.php
[3]: http://www.lepton-cms.com/lepador/snippets/rating.php